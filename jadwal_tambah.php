<!DOCTYPE html>
<html>
<head>
	<title>Tugas PHP</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<h2 style="text-align: center;">Tambah Jadwal</h2>
		<form action="jadwal_act.php" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label>ID Dosen</label>
				<input type="text" class="form-control" placeholder="" name="id_dosen" required="required">
			</div>

            <div class="form-group">
				<label>ID Kelas</label>
				<input type="text" class="form-control" placeholder="" name="id_kelas" required="required">
			</div>

            <div class="form-group">
				<label>Matakuliah</label>
				<input type="text" class="form-control" placeholder="" name="matakuliah" required="required">
			</div>

            <div class="form-group">
				<label>Jadwal</label>
				<input type="date" class="form-control" placeholder="" name="jadwal" required="required">
			</div>
            
            
						
			<input type="submit" name="save" value="Simpan" class="btn btn-primary">
		</form>
	</div>
 
</body>
</html>