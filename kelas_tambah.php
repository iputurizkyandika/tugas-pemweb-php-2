<!DOCTYPE html>
<html>
<head>
	<title>Tugas PHP</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<h2 style="text-align: center;">Tambah Kelas</h2>
		<form action="kelas_act.php" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label>Kelas </label>
				<input type="text" class="form-control" placeholder="" name="kelas" required="required">
			</div>

            <div class="form-group">
				<label>Prodi </label>
				<input type="text" class="form-control" placeholder="" name="prodi" required="required">
			</div>

            <div class="form-group">
				<label>Fakultas </label>
				<input type="text" class="form-control" placeholder="" name="fakultas" required="required">
			</div>
						
			<input type="submit" name="save" value="Simpan" class="btn btn-primary">
		</form>
	</div>
 
</body>
</html>